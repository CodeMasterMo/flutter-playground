import 'package:flutter/material.dart';

class TextControl extends StatelessWidget {
  final Function incrementNumber;

  TextControl(this.incrementNumber);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: RaisedButton(
        child: Text('Increase number'),
        onPressed: incrementNumber,
      ),
    );
  }
}
