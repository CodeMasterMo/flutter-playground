import 'package:flutter/material.dart';

class TextPretty extends StatelessWidget {

  final String text;

  TextPretty(this.text);


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(15),
      width: double.infinity,
      child: Text(text, textAlign: TextAlign.center, style: TextStyle(fontSize: 32),),
    );
  }
}
