import 'package:flutter/material.dart';


import './text_control.dart';
import './text_pretty.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
@override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  int _counter = 0;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Assignment 1",
      home: Scaffold(
        appBar: AppBar(
          title: Text('Flutter Assignment 1'),
        ),
        body: Column(
          children: <Widget>[
            TextPretty(_counter.toString()),
            TextControl(incrementNumber),
          ],
        ),
      ),
    );
  }

  void incrementNumber() {
    setState(() {
      _counter ++;
    });

    print(_counter.toString());
  }
}
